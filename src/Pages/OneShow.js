import React from "react";
import PropTypes from 'prop-types';
import '../Styles/OneShow.css';
import {logDOM} from "@testing-library/react";
import ReactPaginate from 'react-paginate';

import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect, useRouteMatch} from "react-router-dom";


const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);

function OneShow(props) {
    let [UserName, setUserName] = React.useState([]);
    let [UserLastName, setUserLastName] = React.useState([]);
    let [UserAge, setUserAge] = React.useState([]);
    let [UserGender, setUserGender] = React.useState([]);
    let [UserCity, setUserCity] = React.useState([]);
    let [CurrentUser, setCurrentUser] = React.useState([]);
    let [UsersUID, setUsersUID] = React.useState([]);

    let [ShowHeading, setShowHeading] = React.useState([]);
    let [ShowImg, setShowImg] = React.useState([]);
    let [ShowDescription, setShowDescription] = React.useState([]);
    let [ShowNetwork, setShowNetwork] = React.useState([]);
    let [ShowSchedule, setShowSchedule] = React.useState([]);
    let [ShowStatus, setShowStatus] = React.useState([]);
    let [ShowType, setShowType] = React.useState([]);
    let [ShowGenres, setShowGenres] = React.useState([]);
    let [ShowSite, setShowSite] = React.useState([]);
    let [ShowRating, setShowRating] = React.useState([]);

    let [CurrentShow, setCurrentShow] = React.useState([]);

    let Match = useRouteMatch('/show/:id');
    let SelectedShow = (Match?.params?.id) ? Match?.params?.id : 1;

    if (!localStorage.getItem('uid')) {
        window.location.replace('/main');
    }

    function CleanLocalStorage() {
        localStorage.removeItem('uid');
    }

    React.useEffect(() => {
        GetCurrentUserInfo();
        GetSelectedShow(SelectedShow);
        SetUid()
    }, []);

    function SetUid() {
        setUsersUID(UsersUID = localStorage.getItem('uid'));
    }

    function GetSelectedShow(ShowId) {
        fetch(`https://api.tvmaze.com/shows/${ShowId}`)
            .then(res => res.json())
            .then((Show) => {
                    let Schedule = `${Show?.schedule?.days.join(', ')} at ${Show?.schedule?.time} (${Show?.runtime} min)`
                    setCurrentShow(CurrentShow = Show);
                    console.log(CurrentShow);
                    setShowHeading(ShowHeading = Show?.name);
                    setShowImg(ShowImg = Show?.image?.medium);
                    setShowDescription(ShowDescription = Show.summary.replace(/(<([^>]+)>)/gi, ""));
                    setShowNetwork(ShowNetwork = Show?.network?.name);
                    setShowSchedule(ShowSchedule = Schedule);
                    setShowStatus(ShowStatus = Show?.status);
                    setShowType(ShowType = Show?.type);
                    setShowGenres(ShowGenres = Show?.genres.join(' | '));
                    setShowSite(ShowSite = Show?.officialSite);
                    setShowRating(ShowRating = Show?.rating?.average);
                },
            );
        window.scrollTo(0, 0);
    }

    function GetCurrentUserInfo() {
        let ref = firebase.database().ref();
        ref.on("value", function (snapshot) {
            let Users = snapshot.val();
            let refPath = 'user_' + localStorage.getItem('uid');
            setCurrentUser(CurrentUser = Users.users[refPath]);
            setUserName(UserName = CurrentUser.name);
            setUserLastName(UserLastName = CurrentUser.last_name);
            setUserAge(UserAge = CurrentUser.age);
            setUserGender(UserGender = CurrentUser.gender);
            setUserCity(UserCity = CurrentUser.city);
        }, function (error) {
            console.log("Error: " + error.code);
        });
    }

    function Like(Event) {
        GetCurrentUserInfo();
        let refPath = 'users/user_' + UsersUID + '/likes';
        if (!CurrentUser?.likes) {
            alert('Success');
            firebase.database().ref(refPath).set([CurrentShow]);
        } else {
            let IsShowLiked = CurrentUser.likes.filter(function (Show) {
                return parseInt(Show.id) === parseInt(CurrentShow.id);
            });

            if (IsShowLiked.length === 0) {
                CurrentUser.likes.push(CurrentShow);
                firebase.database().ref(refPath).set(CurrentUser.likes);
                alert('Success');
            } else {
                alert('You already liked it before');
            }
        }
    }

    function Follow(Event) {
        GetCurrentUserInfo();
        let refPath = 'users/user_' + UsersUID + '/follows';
        if (!CurrentUser?.follows) {
            alert('Success');
            firebase.database().ref(refPath).set([CurrentShow]);
        } else {
            let IsShowFollowed = CurrentUser.follows.filter(function (Show) {
                return parseInt(Show.id) === parseInt(CurrentShow.id);
            });

            if (IsShowFollowed.length === 0) {
                CurrentUser.follows.push(CurrentShow);
                firebase.database().ref(refPath).set(CurrentUser.follows);
                alert('Success');
            } else {
                alert('You already followed it before');
            }
        }
    }

    return (
        <div>
            <header className="header show-list-header one-show-header">
                <div className="container header-container">
                    <div className="header-top">
                        <div className="top-icon">MY STREAMING</div>
                        <div className="top-items One-Show-Top-items">
                            <div className="top-user">
                                <div className="user-like">
                                    <a href={'/likes'}>
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="heart"
                                             className="svg-inline--fa fa-heart fa-w-16" role="img"
                                             xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                  d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                        </svg>
                                    </a>
                                </div>
                                <div className="user-icon dropdown">
                                    <div className="dropdown-content">
                                        <div className="dropdown-user">
                                            <div className="dropdown__user-photo"/>
                                            <div className="dropdown__user-name">{`${UserName} ${UserLastName}`}</div>
                                            <div className={'dropdown__user-info'}>
                                                <div className="dropdown__user-age">Age: {UserAge} year(s)</div>
                                                <div className="dropdown__user-gender">Gender: {UserGender}</div>
                                                <div className="dropdown__user-city">City: {UserCity}</div>
                                            </div>
                                        </div>
                                        <div className="dropdown-personal">
                                            <div className="dropdown__personal-follows">
                                                <a href={'/follows'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="far"
                                                         data-icon="bookmark"
                                                         className="svg-inline--fa fa-bookmark fa-w-12" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                                        <path fill="currentColor"
                                                              d="M336 0H48C21.49 0 0 21.49 0 48v464l192-112 192 112V48c0-26.51-21.49-48-48-48zm0 428.43l-144-84-144 84V54a6 6 0 0 1 6-6h276c3.314 0 6 2.683 6 5.996V428.43z"/>
                                                    </svg>
                                                    Follows
                                                </a>
                                            </div>
                                            <div className="dropdown__personal-friends">
                                                <a href={'/friends'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="user-plus"
                                                         className="svg-inline--fa fa-user-plus fa-w-20" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                              d="M624 208h-64v-64c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v64h-64c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h64v64c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-64h64c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zm-400 48c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/>
                                                    </svg>
                                                    Friends
                                                </a>
                                            </div>
                                            <div className="dropdown__personal-friends" onClick={CleanLocalStorage}>
                                                <a href={'/main'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="sign-out-alt"
                                                         className="svg-inline--fa fa-sign-out-alt fa-w-16" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                              d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"/>
                                                    </svg>
                                                    Sign out
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main className="main show-list-main">
                <div className="container">
                    <div className={'one-show'}>
                        <div className="shows-heading">
                            <div className={'shows__heading-shows'}><a href={'/shows'}>Shows</a></div>
                            <div>|</div>
                            <div className={'shows__heading-People'}><a href={'/people'}>People</a></div>
                        </div>
                        <hr className="shows-hr"/>
                        <div className={'one__show-name'}>{ShowHeading}</div>
                        <div className={'show-items'}>
                            <div className={'show__items-img'}>
                                <div>
                                    <img
                                        src={ShowImg}/>
                                </div>
                                <div className={'show__items-btn'}>
                                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="heart"
                                         className="svg-inline--fa fa-heart fa-w-16" role="img"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" onClick={Like}>
                                        <path fill="currentColor"
                                              d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                    </svg>
                                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="bookmark"
                                         className="svg-inline--fa fa-bookmark fa-w-12" role="img"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" onClick={Follow}>
                                        <path fill="currentColor"
                                              d="M336 0H48C21.49 0 0 21.49 0 48v464l192-112 192 112V48c0-26.51-21.49-48-48-48zm0 428.43l-144-84-144 84V54a6 6 0 0 1 6-6h276c3.314 0 6 2.683 6 5.996V428.43z"/>
                                    </svg>
                                </div>
                            </div>

                            <div className={'show__items-description'}>
                                <div className={'show__items-description-text'}>{ShowDescription}</div>
                                <div className={'show__items-sharing'}>
                                    <div className={'show__items__sharing-heading'}>Share this on:</div>
                                    <div className={'show__items__sharing-btn'}>
                                        <div className={'twitter-svg-sharing'}>
                                            <a href={`https://twitter.com/share`}>
                                                <svg aria-hidden="true" focusable="false" data-prefix="fab"
                                                     data-icon="twitter-square"
                                                     className="svg-inline--fa fa-twitter-square fa-w-14" role="img"
                                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                          d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-48.9 158.8c.2 2.8.2 5.7.2 8.5 0 86.7-66 186.6-186.6 186.6-37.2 0-71.7-10.8-100.7-29.4 5.3.6 10.4.8 15.8.8 30.7 0 58.9-10.4 81.4-28-28.8-.6-53-19.5-61.3-45.5 10.1 1.5 19.2 1.5 29.6-1.2-30-6.1-52.5-32.5-52.5-64.4v-.8c8.7 4.9 18.9 7.9 29.6 8.3a65.447 65.447 0 0 1-29.2-54.6c0-12.2 3.2-23.4 8.9-33.1 32.3 39.8 80.8 65.8 135.2 68.6-9.3-44.5 24-80.6 64-80.6 18.9 0 35.9 7.9 47.9 20.7 14.8-2.8 29-8.3 41.6-15.8-4.9 15.2-15.2 28-28.8 36.1 13.2-1.4 26-5.1 37.8-10.2-8.9 13.1-20.1 24.7-32.9 34z"/>
                                                </svg>
                                            </a>
                                        </div>
                                        <div>
                                            <a href={`https://www.facebook.com/sharer.php?u=/show/${CurrentShow.id}`}>
                                                <svg aria-hidden="true" focusable="false" data-prefix="fab"
                                                     data-icon="facebook-square"
                                                     className="svg-inline--fa fa-facebook-square fa-w-14" role="img"
                                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    {/*<use xlink:href={`/show/${CurrentShow.id}`}>*/}
                                                        <path fill="currentColor"
                                                              d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"/>
                                                    {/*</use>*/}
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={'show__items-info'}>
                                <div className={'show__items__info__item-heading'}>Show Info:</div>
                                <div className={'show__items__info-item'}>Network: {ShowNetwork}</div>
                                <div className={'show__items__info-item'}>Schedule: {ShowSchedule}</div>
                                <div className={'show__items__info-item'}>Status: {ShowStatus}</div>
                                <div className={'show__items__info-item'}>Show Type: {ShowType}</div>
                                <div className={'show__items__info-item'}>Genres: {ShowGenres}</div>
                                <div className={'show__items__info-item'}>Official site: <a
                                    href={ShowSite}>{ShowSite}</a></div>
                                <div className={'show__items__info__item-rating'}>
                                    <div>
                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star"
                                             className="svg-inline--fa fa-star fa-w-18" role="img"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/>
                                        </svg>
                                    </div>
                                    <div>7.5</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer className="footer">
                <div className="container footer-container">
                    <div className="footer-info">
                        <div className="footer__info-heading">Have questions? Contact us.</div>
                        <div className="footer__info-items">
                            <div className="items-block">
                                <p>Common questions</p>
                                <p>For investors</p>
                                <p>Confidentiality</p>
                                <p>Speed check</p>
                            </div>
                            <div className="items-block">
                                <p>Support Center</p>
                                <p>Vacancies</p>
                                <p>Cookie settings</p>
                                <p>Legal Notices</p>
                            </div>
                            <div className="items-block bottom-item">
                                <p>Account</p>
                                <p>Viewing methods</p>
                                <p>Corporate information</p>
                                <p>Only on MY STREAM</p>
                            </div>
                            <div className="items-block bottom-item">
                                <p>Media center</p>
                                <p>Terms of use</p>
                                <p>Contact us</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}

OneShow.propTypes = {};

export default OneShow;