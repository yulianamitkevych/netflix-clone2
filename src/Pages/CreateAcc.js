import React from "react";
import PropTypes from 'prop-types';
import '../Styles/Login.css'
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect} from "react-router-dom";

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);

function Registration(props) {
    const auth = getAuth();

    let [Name, setName] = React.useState([]);
    let [LastName, setLastName] = React.useState([]);
    let [Gender, setGender] = React.useState([]);
    let [Age, setAge] = React.useState([]);
    let [City, setCity] = React.useState([]);
    let [UserLogin, setUserLogin] = React.useState([]);
    let [Password, setPassword] = React.useState([]);
    let [ErrorMessage, setErrorMessage] = React.useState([]);

    let [UsersUID, setUsersUID] = React.useState([]);

    let [CreatedUserEmail, setCreatedUserEmail] = React.useState([]);
    let [UserRefreshToken, setUserRefreshToken] = React.useState([]);

    if (localStorage.getItem('uid')) {
        window.location.replace('/shows');
    }

    function UserRegistration(Name, LastName, Gender, Age, City, UserLogin, Password) {
        setErrorMessage(ErrorMessage = '');
        firebase.auth().createUserWithEmailAndPassword(UserLogin, Password).then(function (value) {
            setCreatedUserEmail(CreatedUserEmail = value.user.email);
            setUserRefreshToken(UserRefreshToken = value.user.refreshToken);
            localStorage.setItem('uid', firebase.auth().currentUser._delegate.uid);
            CreateUserInDB(Name, LastName, Gender, Age, City, firebase.auth().currentUser._delegate.uid);
            window.location.replace('/shows');
        }).catch(function (value) {
            setTimeout(function () {
                setErrorMessage(ErrorMessage = 'Invalid Email or Password');
            }, 1000);
        });
    }

    function ApplyRegistration() {
        UserRegistration(Name, LastName, Gender, Age, City, UserLogin, Password, UsersUID);
    }

    function CreateUserInDB(Name, LastName, Gender, Age, City, Uid) {
        let refPath = 'users/user_' + Uid;
        firebase.database().ref(refPath).set({
            name: Name,
            last_name: LastName,
            gender: Gender,
            age: Age,
            city: City
        });
    }

    function getId() {
        setUsersUID(firebase.auth().currentUser);
    }

    function GetName(Event) {
        setName(Event.target.value);
    }

    function GetLastName(Event) {
        setLastName(Event.target.value);
    }

    function GetGender(Event) {
        setGender(Event.target.value);
    }

    function GetAge(Event) {
        setAge(Event.target.value);
    }

    function GetCity(Event) {
        setCity(Event.target.value);
    }

    function GetLogin(Event) {
        setUserLogin(Event.target.value);
    }

    function GetPassword(Event) {
        setPassword(Event.target.value);
    }

    return (
        <div>
            <header className='header enter-header' id={'enter-header'}>
                <div className="container header-container">
                    <div className="header-top">
                        <div className="top-icon">MY STREAMING</div>
                    </div>
                </div>
            </header>
            <main className="main enter-main">
                <div className="container">
                    <div className="enter-group">
                        <div className="enter-text">Registration</div>
                        <div className="email-error">{ErrorMessage}</div>
                        <div className="enter-input">
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="user-name" name="name" placeholder="Name"
                                       type="text" onChange={GetName}/>
                            </div>
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="user-lastName" name="lastName" placeholder="Last Name"
                                       type="text" onChange={GetLastName}/>
                            </div>
                            <div className="header__main-email enter-email">
                                <div className={'gender-age'}>
                                    <div className="header__main-email enter-email">
                                        <label/>
                                        <input id="gender" name="gender" placeholder="Gender"
                                               type="text" onChange={GetGender}/>
                                    </div>
                                    <div className="header__main-email enter-email">
                                        <label/>
                                        <input id="age" name="age" placeholder="Age"
                                               type="number" onChange={GetAge}/>
                                    </div>
                                </div>
                            </div>
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="city" name="city" placeholder="City"
                                       type="text" onChange={GetCity}/>
                            </div>
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="header-email" name="email" placeholder="Email"
                                       type="email" onChange={GetLogin}/>
                            </div>
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="password" name="password" placeholder="Password"
                                       type="password" onChange={GetPassword}/>
                            </div>
                        </div>
                        <div className="enter-btn" onClick={ApplyRegistration}>Submit</div>
                    </div>
                </div>
            </main>
            <footer className="footer">
                <div className="container footer-container">
                    <div className="footer-info">
                        <div className="footer__info-heading">Have questions? Contact us.</div>
                        <div className="footer__info-items">
                            <div className="items-block">
                                <p>Common questions</p>
                                <p>For investors</p>
                                <p>Confidentiality</p>
                                <p>Speed check</p>
                            </div>
                            <div className="items-block">
                                <p>Support Center</p>
                                <p>Vacancies</p>
                                <p>Cookie settings</p>
                                <p>Legal Notices</p>
                            </div>
                            <div className="items-block bottom-item">
                                <p>Account</p>
                                <p>Viewing methods</p>
                                <p>Corporate information</p>
                                <p>Only on MY STREAM</p>
                            </div>
                            <div className="items-block bottom-item">
                                <p>Media center</p>
                                <p>Terms of use</p>
                                <p>Contact us</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}

Registration.propTypes = {};

export default Registration;