import React from "react";
import PropTypes from 'prop-types';
import '../Styles/Login.css'

import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect} from "react-router-dom";


const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);


function Login(props) {
    let [GetUserLogin, setGetUserLogin] = React.useState([]);
    let [GetUserPassword, setGetUserPassword] = React.useState([]);
    let [ErrorMessage, setErrorMessage] = React.useState([]);
    let [CreatedUserEmail, setCreatedUserEmail] = React.useState([]);
    let [UserRefreshToken, setUserRefreshToken] = React.useState([]);

    if (localStorage.getItem('uid')) {
        window.location.replace('/shows');
    }

    function SignIn(Email, Password) {
        firebase.auth().signInWithEmailAndPassword(Email, Password)
            .then(function (firebaseUser) {
                setCreatedUserEmail(CreatedUserEmail = firebaseUser.user.email);
                setUserRefreshToken(UserRefreshToken = firebaseUser.user.refreshToken)
                localStorage.setItem('email', CreatedUserEmail);
                localStorage.setItem('refreshToken', UserRefreshToken);
                window.location.replace('/shows');
            })
            .catch(function (error) {
                setTimeout(function () {
                    setErrorMessage(ErrorMessage = 'Invalid Email or Password');
                }, 1000);
            });
    }

    function ApplySignIn() {
        SignIn(GetUserLogin, GetUserPassword);
    }

    function GetLogin(Event) {
        setGetUserLogin(Event.target.value);
    }

    function GetPassword(Event) {
        setGetUserPassword(Event.target.value);
    }

    return (
        <div>
            <header className='header enter-header' id={'enter-header'}>
                <div className="container header-container">
                    <div className="header-top">
                        <div className="top-icon">MY STREAMING</div>
                    </div>
                </div>
            </header>
            <main className="main enter-main">
                <div className="container">
                    <div className="enter-group">
                        <div className="enter-text">Sign in</div>
                        <div className="enter-input">
                            <div className="email-error">{ErrorMessage}</div>
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="header-email" name="email" placeholder="Email"
                                       type="email" onChange={GetLogin}/>
                            </div>
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="sign-in-pw" name="password" placeholder="Password"
                                       type="password" onChange={GetPassword}/>
                            </div>
                        </div>
                        <div className="enter-btn" onClick={ApplySignIn}>Sign in</div>
                    </div>
                </div>
            </main>
            <footer className="footer">
                <div className="container footer-container">
                    <div className="footer-info">
                        <div className="footer__info-heading">Have questions? Contact us.</div>
                        <div className="footer__info-items">
                            <div className="items-block">
                                <p>Common questions</p>
                                <p>For investors</p>
                                <p>Confidentiality</p>
                                <p>Speed check</p>
                            </div>
                            <div className="items-block">
                                <p>Support Center</p>
                                <p>Vacancies</p>
                                <p>Cookie settings</p>
                                <p>Legal Notices</p>
                            </div>
                            <div className="items-block bottom-item">
                                <p>Account</p>
                                <p>Viewing methods</p>
                                <p>Corporate information</p>
                                <p>Only on MY STREAM</p>
                            </div>
                            <div className="items-block bottom-item">
                                <p>Media center</p>
                                <p>Terms of use</p>
                                <p>Contact us</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}

Login.propTypes = {}

export default Login;